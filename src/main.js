import { createApp } from 'vue';
import App from './App.vue'; // Import the App component

const app = createApp(App); // Pass the App component to createApp

app.mount('#app');

